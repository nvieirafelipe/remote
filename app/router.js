import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('about', { path: '/about' });
  this.route('workdays', { path: '/workdays' });
  this.route('developers', { path: '/developers' });
});

export default Router;
