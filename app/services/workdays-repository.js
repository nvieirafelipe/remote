import Ember from 'ember';

export default Ember.Service.extend({
  store: Ember.inject.service('store'),

  addDeveloperToWorkday(developer_id, workday) {
    this.get('store').find('developer', developer_id).then(function(developer) {
      var developers = workday.get('developers');
      developers.addObject(developer);
      workday.save();

      developer.get('workdays').then(function(workdays) {
        developer.set('workdaysCount', workdays.length);
        developer.save();
      });
    });
  }
});
