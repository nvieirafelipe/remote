import Ember from 'ember';

export default Ember.Component.extend({
  developersSorting: ['name'],
  developersSortedByName: Ember.computed.sort('workday.developers', 'developersSorting'),
  tagName: 'li',

  dragEnter(e) {
    this.dragOverStyle(e.target);
  },

  dragOver(e) {
    e.preventDefault();
  },

  dragLeave(e) {
    this.clearDragOverStyle(e.target);
  },

  drop(e) {
    this.clearDragOverStyle(e.target);
    var workday = this.get('workday');
    var developer_id = e.dataTransfer.getData('text/plain');
    this.sendAction('action', developer_id, workday);
  },

  // ---

  dragOverStyle(element) {
    element.style.background = '#333';
    element.style.color = '#FFF';
  },

  clearDragOverStyle(element) {
    element.style.background = '';
    element.style.color = '';
  }
});
