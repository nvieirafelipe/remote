import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  attributeBindings: ['draggable'],

  dragStart: function(e) {
    e.dataTransfer.effectAllowed = 'all';
    e.dataTransfer.setData('text', this.get('developer').id);
  }
});
