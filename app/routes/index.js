import Ember from 'ember';

export default Ember.Route.extend({
  workdaysRepository: Ember.inject.service('workdays-repository'),

  model() {
    return Ember.RSVP.hash({
      workdays: this.store.findAll('workday'),
      developers: this.store.findAll('developer')
    });
  },

  actions: {
    addDeveloperToWorkday(developer_id, workday) {
      this.get('workdaysRepository').addDeveloperToWorkday(developer_id, workday)
    }
  }
});

