import DS from 'ember-data';

export default DS.Model.extend({
  date: DS.attr('date'),
  developers: DS.hasMany('developer', { async: true }),
  localeDate: Ember.computed('date', function() {
    return this.get('date').toLocaleDateString('pt-BR');
  })
});
