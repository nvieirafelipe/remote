import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  workdays: DS.hasMany('workday', { async: true }),
  workdaysCount: DS.attr('number'),
  initials: Ember.computed('name', function() {
    var words = this.get('name').split(' ');
    return words[0][0] + words[1][0];
  })
});
