import Ember from 'ember';

export default Ember.Controller.extend({
  developersSort: ['name'],
  developers: Ember.computed.sort('model', 'developersSort'),
  actions: {
    createDeveloper() {
      var newDeveloper = this.store.createRecord('developer', {
        name: this.get('name'),
        workdaysCount: 0
      });
      newDeveloper.save();
    }
  }
});
