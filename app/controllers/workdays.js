import Ember from 'ember';

export default Ember.Controller.extend({
  workdaysSort: ['date'],
  workdays: Ember.computed.sort('model', 'workdaysSort'),
  actions: {
    createWorkday() {
      var newWorkday = this.store.createRecord('workday', {
        date: new Date(this.get('date'))
      });
      newWorkday.save();
    }
  }
});
