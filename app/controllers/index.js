import Ember from 'ember';

export default Ember.Controller.extend({
  developersSort: ['workdaysCount', 'name'],
  developers: Ember.computed.sort('model.developers', 'developersSort'),

  workdaysSort: ['date'],
  workdays: Ember.computed.sort('model.workdays', 'workdaysSort')
});
